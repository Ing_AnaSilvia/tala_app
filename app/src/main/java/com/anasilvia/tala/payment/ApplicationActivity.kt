package com.anasilvia.tala.payment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.anasilvia.tala.R
import com.anasilvia.tala.models.User
import com.anasilvia.tala.utils.Utils
import kotlinx.android.synthetic.main.activity_application.*

class ApplicationActivity : AppCompatActivity() {

    var user: User?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_application)
        user = intent.extras!!.getSerializable("user")!! as User

        (user!!.currency + user!!.loanLimit).also { tv_max_ammount.text = it }
    }

    fun sendApplication(view: View?){
        if(et_name.text.isNotEmpty() && et_number.text.isNotEmpty() && et_address.text.isNotEmpty()){
            Utils().showAlertDialog("Your Application has been sent successfully",this)
        }else{
            //TODO: SHOW THE MISSING FILEDS
        }
    }
}