package com.anasilvia.tala.payment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.anasilvia.tala.R
import com.anasilvia.tala.models.User
import com.anasilvia.tala.utils.Utils
import kotlinx.android.synthetic.main.activity_payment.*

class PaymentActivity : AppCompatActivity() {
    var user: User?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        user = intent.extras!!.getSerializable("user")!! as User

        initControls()
    }

    fun initControls(){
        tv_max_ammount.text = user!!.currency + user!!.due
        button_pay.setOnClickListener {
            if(validateFields()){
                makePayment()
            }else if(et_name.text.toString().length == 16){
                Utils().showAlertDialog("You must provide the 16 digits card number",this)
            }else if(et_venc.text.toString().length != 4){
                Utils().showAlertDialog("You must provide the 4 digits card expiration",this)
            }
            else if(et_number.text.toString().length != 3){
                Utils().showAlertDialog("You must provide the 3 secret card digits",this)
            }
        }

    }

    fun validateFields():Boolean{
        if(et_name.text.toString().length == 16 && et_venc.text.toString().length == 4 && et_venc.text.toString().length == 4){
            return true
        }else{
            return false
        }
    }

    fun makePayment(){
       tv_max_ammount.text = user!!.currency + "0"
       Utils().showAlertDialog("Payment Aprobed",this)
    }

}