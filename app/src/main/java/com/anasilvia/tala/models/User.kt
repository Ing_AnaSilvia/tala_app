package com.anasilvia.tala.models

import android.os.Parcelable
import android.util.Log
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.json.JSONObject
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

open class User: RealmObject,Serializable{

    var locale:String?=null
    var status: String?=null
    var level: String?=null
    var due: String?=null
    var dueDate: Long?=null
    var timestamp: String?=null
    @PrimaryKey
    var userName: String?=null
    var aprovedAmmount: String?=null
    var currency:String?=null
    var loanLimit:String?=null

    constructor(json: JSONObject){
        try{
            this.locale = json.getString("locale")
            this.userName = json.getString("username")
            this.timestamp = json.getString("timestamp")
            val loan = json.getJSONObject("loan")
            this.status = loan.getString("status")
            this.level = loan.getString("level")
            this.due = loan.optString("due")
            this.dueDate = loan.getLong("dueDate")
            this.aprovedAmmount = loan.optString("approved")
        }catch (er: Exception){
            Log.wtf("error","$er")
        }
    }

    constructor()

    fun dueDateFormatted(): String{
        var netDate : Date
        var sdf = ""
        try {
            val sdf = SimpleDateFormat("MM/dd/yyyy")
            netDate = Date(dueDate!! * 1000)
            return sdf.format(netDate)
        } catch (e: Exception) {
            return e.toString()
        }
        return sdf
    }
}