package com.anasilvia.tala.utils

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.anasilvia.tala.R
import java.io.IOException

class Utils {
    var myDialog: AlertDialog? = null

    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }


    fun showAlertDialog(msj: String, ctx: Context?) {
        val alertView: View
        val acept: Button
        val builder = AlertDialog.Builder(ctx)
        val inflater = LayoutInflater.from(ctx)
        alertView = inflater.inflate(R.layout.dialog, null)
        val message = alertView.findViewById<View>(R.id.textMessageB) as TextView
        message.text = "" + msj
        builder.setView(alertView)
        myDialog = builder.create()
        acept = alertView.findViewById<View>(R.id.aceptarBtn) as Button
        acept.setOnClickListener { myDialog!!.dismiss() }
        if (!myDialog!!.isShowing()) {
            myDialog!!.show()
        }
    }

}