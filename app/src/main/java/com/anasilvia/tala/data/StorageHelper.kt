package com.anasilvia.tala.data

import android.content.Context
import com.anasilvia.tala.models.User
import io.realm.Realm
import io.realm.RealmQuery
import io.realm.RealmResults

class StorageHelper{
    var realm: Realm?= null

    constructor(context:Context){
        // Initialize Realm
        Realm.init(context)
        realm = Realm.getDefaultInstance()
    }

    fun setUserData(ctx: Context, user:User) {
        try {
            realm!!.beginTransaction()
            realm!!.insertOrUpdate(user)
            realm!!.commitTransaction()
        } finally {
            realm!!.close();
        }
    }

    fun getUserData(ctx: Context): User{
        val query: RealmQuery<User> = realm!!.where<User>(User::class.java)
        // Execute the query:
        val result1: RealmResults<User>? = query.findAll()
        return result1!!.last()!!
    }


}