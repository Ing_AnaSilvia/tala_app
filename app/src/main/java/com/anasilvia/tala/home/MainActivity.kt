package home

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.anasilvia.tala.R
import com.anasilvia.tala.models.User
import com.anasilvia.tala.payment.ApplicationActivity
import com.anasilvia.tala.payment.PaymentActivity
import com.anasilvia.tala.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
   var user: User?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        user = intent.extras!!.getSerializable("user")!! as User
        configFields()
    }

    fun configFields(){
        if(user!!.level != null){
            modifyImageLevel(user!!.level)
            modifyImageStory(user!!.locale)
            modifyButtons(user!!.status)
            modifyTextLabels()
        }else{
          btn_apply_now.visibility = View.VISIBLE
          tv_loan.text = getString(R.string.qualify_info, user!!.currency, user!!.loanLimit)
          tv_grow.text = getString(R.string.track_process)
        }

    }

    fun modifyTextLabels(){
        tv_grow.text = getString(R.string.grow_limit, user!!.currency, user!!.loanLimit)

        when (user!!.status){
            "paid" -> {
                tv_aply_loan.text = getString(R.string.fully_paid)
                tv_loan.text = getString(R.string.apply_another)
            }
            "due" -> {
                (user!!.currency + user!!.due).also { tv_due.text = it }
                tv_aply_loan.text = getString(R.string.on_track)
                tv_loan.text = getString(R.string.is_due, user!!.dueDateFormatted())
            }
            "approved" -> {
                tv_aply_loan.text = getString(R.string.app_aproved)
                tv_loan.text = getString(
                    R.string.aproved_quantity,
                    user!!.currency,
                    user!!.aprovedAmmount
                )
            }
        }
    }

    fun modifyImageLevel(level: String?){
        when (level){
            "gold" -> image_level.setImageDrawable(getDrawable(R.drawable.img_gold_badge_large))
            "bronze" -> image_level.setImageDrawable(getDrawable(R.drawable.img_bronze_badge_large))
            "silver" -> image_level.setImageDrawable(getDrawable(R.drawable.img_silver_badge_large))
        }
    }

    fun modifyImageStory(local: String?){
        when (local){
            "mx" -> image_history.setBackgroundResource(R.drawable.img_story_card_mx)
            "ke" -> image_history.setBackgroundResource(R.drawable.img_story_card_ke)
            "ph" -> image_history.setBackgroundResource(R.drawable.img_story_card_ph)
        }
    }

    fun modifyButtons(local: String?){
        when (local){
            "paid" -> {
                image_header.visibility = View.VISIBLE
                image_header.setImageDrawable(getDrawable(R.drawable.img_loan_status_paidoff))
                btn_apply_now.visibility = View.VISIBLE
                btn_make_payment.visibility = View.GONE
                btn_view_loan.visibility = View.GONE
                btn_reply.visibility = View.GONE
            }
            "due" -> {
                val layoutParams =
                    ConstraintLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, // CardView width
                        640
                    )
                cardView2.layoutParams = layoutParams
                btn_apply_now.visibility = View.GONE
                header.visibility = View.GONE
                btn_make_payment.visibility = View.VISIBLE
                btn_view_loan.visibility = View.GONE
                btn_reply.visibility = View.VISIBLE
                tv_due.visibility = View.VISIBLE
            }
            "approved" -> {
                btn_apply_now.visibility = View.GONE
                image_header.setImageDrawable(getDrawable(R.drawable.img_loan_status_approved))
                btn_view_loan.visibility = View.VISIBLE
                btn_reply.visibility = View.GONE
                btn_make_payment.visibility = View.GONE
            }
        }
    }

    fun reply(view: View){

    }

    fun makePayment(view: View){
        startActivity(Intent(this, PaymentActivity::class.java).putExtra("user", user!!))
    }

    fun viewLoan(view: View){
    Utils().showAlertDialog("Your application is approved for ${user!!.currency} ${user!!.aprovedAmmount}",this)
    }

    fun applyNow(view: View){
        startActivity(Intent(this, ApplicationActivity::class.java).putExtra("user", user!!))
    }

    fun viewStatus(view: View){
        Utils().showAlertDialog("Your Actual Crédit Status is: ${user!!.status}",this)
    }

    fun readMore(view: View){
        Utils().showAlertDialog(resources.getString(R.string.tala_help), this)
    }

    fun inviteFriends(view: View){
        val intent = Intent(Intent.ACTION_SEND)
        val shareBody = getString(R.string.tala_share)
         intent.type = "text/plain"
         intent.putExtra(
             Intent.EXTRA_SUBJECT,
             getString(R.string.tala_share)
         )
        intent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(intent, getString(R.string.tala_share_title)))
    }

    fun sendUsMessage(view: View){
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_EMAIL, "support@tala.com")
        intent.putExtra(Intent.EXTRA_SUBJECT, "Questions")
        intent.putExtra(Intent.EXTRA_TEXT, "I need some information!")
        startActivity(Intent.createChooser(intent, "Send Email"))
    }
}