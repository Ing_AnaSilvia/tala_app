package com.anasilvia.tala.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.anasilvia.tala.R
import com.anasilvia.tala.data.StorageHelper
import com.anasilvia.tala.models.User
import com.anasilvia.tala.utils.Utils
import home.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONArray
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initControls()
    }

    fun initControls(){
        button_sign_in.setOnClickListener {
          if(validateFields()){
              startLogin()
          }else if(et_email.text.isEmpty()){
              Utils().showAlertDialog("Please fill the email field!",this)
          }else if(et_password.text.isEmpty()){
              Utils().showAlertDialog("Please fill the password field!",this)
          }
        }
    }

    fun validateFields():Boolean{
        return (et_email.text.isNotEmpty() && et_password.text.isNotEmpty())
    }

    fun startLogin(){
     val jsonArray = JSONArray(Utils().getJsonDataFromAsset(this,"locales/testData.json"))

     var userExists = false

     for(i in 0 until jsonArray.length()){
         if(jsonArray.getJSONObject(i).getString("username").equals(et_email.text.toString())){
                userExists = true

                val usuario = User(jsonArray.getJSONObject(i))

                StorageHelper(this).setUserData(this,getLoanLimit(usuario))

                startActivity(Intent(this,MainActivity::class.java).putExtra("user",usuario))
         }
     }

        if(!userExists){
            Utils().showAlertDialog("User doesn´t exists",this)
        }

    }

    fun getLoanLimit(user: User):User{
        val limit:String
        val currency:String

        val jsonObject = JSONObject((Utils().getJsonDataFromAsset(this,"locales/locales.json")!!))

        val myJson = jsonObject.getJSONObject(user.locale!!)

        limit = myJson.getString("loanLimit")
        currency = myJson.getString("currency")

        user.loanLimit = limit
        user.currency = currency

        return user
    }

}